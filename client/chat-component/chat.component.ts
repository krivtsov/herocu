import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Inject } from "@angular/core";
import * as globalVars from "../service/global";

/// <reference path="../../typings/globals/jquery/index.d.ts/>


import "/socket.io/socket.io.js";


@Component({
    moduleId: module.id,
    selector: "chat-page",
    templateUrl: "./chat.component.html",
    styleUrls: ['css/chat.css']
})

export class ChatComponent implements OnInit {
    reference: any;
    resFlag: boolean = false;
    newUser: boolean = false;
    exitedUser: boolean = false;
    newUserName: string = null;
    exitedUserName: string = null;
    sentMessageUsername: string = null;
    response: string;
    clientsNameList: Object;
    message: string;
    msgCount: number = 0;
    count: number = 0;
    currentRoom: string = null;
    timerCount: number = 0;
    timerWeek: number = 0;
    active: boolean = false;
    public show:boolean = true;
    mes = [];
    protected router;
    email: string = null;
    room: string = null;
    timerMonth:number = 0;

    constructor( @Inject(Router) router: Router) {
        this.router = router;
        globalVars.socket = io({ query: "userName=" + 'wef' });
        let reference = this;
        let temp;
        var name = JSON.parse(localStorage.getItem('profile'));
        globalVars.socket.emit("active", name, function (msg) { });
        globalVars.socket.emit('timerUp', name, function (msg, bool) {
            var countToday = 0;
            var countWeek = 0;
            var countMonth = 0;
            for (var i = 0; i < msg.length; i++) {
                var date = new Date(msg[i].date);
                if (date.getDate() === new Date().getDate()) {
                    countToday++;
                }
                if (date.getDate() >= new Date().getDate() - 7) {
                    countWeek++;
                }
                if (date.getMonth() === new Date().getMonth()) {
                    countMonth++;
                }
            }
            reference.timerCount = countToday;
            reference.timerWeek = countWeek;
            reference.timerMonth = countMonth;

            var obj = {
                timerToday: countToday,
                timerWeek: countWeek,
                timerMonth: countMonth,
                name: name.email
            }
            console.log(obj);
            globalVars.socket.emit('UpdateTimer', obj, function (msg, name) {
            });

        });
        globalVars.socket.on("broadcastToAll_chatMessage", function (resObj) {
            reference.mes.push(resObj);
            console.log(resObj);
            // if (reference.sentMessageUsername !== resObj.name) {
            //     resObj.name = resObj.name + ": ";
            //     temp = $("#messages").length;
            //     console.log("ul length : ", temp);
            //     console.log(reference.msgCount);
            //     $("#messages").append($("<li class='li' data-index=" + reference.msgCount + ">"));
            //     $("li[data-index=" + reference.msgCount + "]").append($("<div class='left-msg' data-index=" + reference.msgCount + ">"));
            //     $("div[data-index=" + reference.msgCount + "]").append($("<span class='name'>").text(resObj.name));
            //     $("div[data-index=" + reference.msgCount + "]").append($("<span class='msg'>").text(resObj.msg));
            //     $("#messages").append($("<br>"));

            // }
            // else if (reference.sentMessageUsername === resObj.name) {
            //     $("#messages").append($("<li data-index=" + reference.msgCount + ">"));
            //     $("li[data-index=" + reference.msgCount + "]").append($("<div class='right-msg' data-index=" + reference.msgCount + ">"));
            //     $("div[data-index=" + reference.msgCount + "]").append($("<span class='msg'>").text(resObj.msg));
            //     $("#messages").append($("<br>"));
            //     reference.sentMessageUsername = null;
            // }
        });
        globalVars.socket.emit('mesStory', name);
        globalVars.socket.on('mes_story', function (data) {
            reference.mes = data;
        })
        globalVars.socket.on("addUserToSocketList", function (username) {
            reference.exitedUser = false;
            reference.newUser = true;
            reference.newUserName = username;
        });
        globalVars.socket.on("removeUserFromSocketList", function (username) {
            reference.newUser = false;
            reference.exitedUser = true;
            reference.exitedUserName = username;
        });
        globalVars.socket.on("updateUsers", function (val) {
            reference.count = val;
        });
        globalVars.socket.emit('updateListUsers', name, function (msg, bool) {
            // reference.timerCount = msg.timerToday;
            // reference.timerWeek = msg.timerWeek;
        });
        // globalVars.socket.on('CurrentRoom', function (name) {
        //     reference.currentRoom = name;
        // })

        globalVars.socket.on("updateSocketList", function (list) {
            console.log();
            reference.clientsNameList = list;
            $(".button-collapse").sideNav();
            $('.modal').modal();
            reference.clientsNameList = list;
            $('.modal').modal({
                dismissible: true,
                opacity: .5,
                in_duration: 600,
                out_duration: 500,
                starting_top: '10%',
                ending_top: '4%',
                ready: function (modal, trigger) {
                    console.log(modal, trigger);
                },
                complete: function () { } // Callback for Modal close
            });
            reference.show = false;
        });
    }
    sendMessage(data) {
        this.resFlag = true;
        let reference = this;
        var user = JSON.parse(localStorage.getItem('profile'));
        var obj = {
            message: data.value,
            room: user.room,
            email: user.email,
            login: user.name
        };
        reference.mes.push(obj);
        globalVars.socket.emit("chatMessageToSocketServer", obj, function (respMsg) {
            console.log(respMsg);
        });
        $("#first_name2").val(" ");
    }

    sendMessageOnEnter($event, messagebox) {
        if ($event.which === 13) { // ENTER_KEY
            this.sendMessage(messagebox);
        }
    }

    update() {
        this.resFlag = false;
        this.newUser = false;
        this.exitedUser = false;
    }

    toDashBoard($event) {
        $event.preventDefault();
        this.router.navigate(['']);
    }
    public openModal($event) {
        $event.preventDefault();
        $('#modal1').modal('open');
    }
    public closeModal($event) {
        $event.preventDefault();
        $('#modal1').modal('close');
    }

    public sendEmail(email, room) {
        this.email = $('#email').val();
        this.room = $('#room').val();

        var obj = {
            email: this.email,
            room: this.room
        }

        globalVars.socket.emit('sendEmail', obj, function (res) {

        });
        $('#modal1').modal('close');
    }

    toProgress($event) {
        $event.preventDefault();
        this.router.navigate(['dashboard']);
    }
}