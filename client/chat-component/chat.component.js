"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var core_2 = require("@angular/core");
var globalVars = require("../service/global");
/// <reference path="../../typings/globals/jquery/index.d.ts/>
require("/socket.io/socket.io.js");
var ChatComponent = (function () {

    function ChatComponent(router) {
        this.resFlag = false;
        this.newUser = false;
        this.exitedUser = false;
        this.newUserName = null;
        this.exitedUserName = null;
        this.sentMessageUsername = null;
        this.msgCount = 0;
        this.count = 0;
        this.currentRoom = null;
        this.timerCount = 0;
        this.timerWeek = 0;
        this.timerMonth = 0;
        this.active = false;
        this.mes = [];
        this.email = null;
        this.room = null;
        this.show = true;
        this.router = router;
        var temp;
        this.admin = Boolean;
        globalVars.socket = io('sheltered-springs-88229.herokuapp.com');

    }
    ChatComponent.prototype.ngOnInit = function () {
        var reference = this;
        if (localStorage.getItem('profile')) {
            var name = JSON.parse(localStorage.getItem('profile'));
            reference.currentRoom = name.room;
            this.admin = name.pro_user;
            this.emailAdmin = name.email;

            globalVars.socket.emit("active", name, function (msg) { });
            globalVars.socket.emit("login_", name, function (res, user) {
                reference.timerCount = user.tymerToday;
                reference.timerWeek = user.tymerWeek;
                reference.timerMonth = user.typerMonth;
                console.log('login user done');
            });
            globalVars.socket.on("broadcastToAll_chatMessage", function (resObj) {
                console.log('broadcastToAll_chatMessage done');
                reference.mes.push(resObj);
            });
            globalVars.socket.emit('mesStory', name);
            globalVars.socket.on('mes_story', function (data) {
                reference.mes = data;
                console.log('mes_story done');
            });
            globalVars.socket.on("updateUsers", function (val) {
                reference.count = val;
                console.log('updateUsers done');
            });
            globalVars.socket.emit('updateListUsers', name, function (msg, bool) {
                console.log('updateListUsers done');
            });
            globalVars.socket.on('del', function(data){
                console.log(data);
                if(data.email === name.email)
                    reference.router.navigate(['']);
            })
            globalVars.socket.on("updateSocketList", function (list) {
                console.log("updateSocketList done");
                $(".button-collapse").sideNav();
                reference.clientsNameList = list;
                $('.modal').modal();

                $('.modal').modal({
                    dismissible: true,
                    opacity: .5,
                    in_duration: 600,
                    out_duration: 500,
                    starting_top: '10%',
                    ending_top: '4%',
                    ready: function (modal, trigger) {
                    },
                    complete: function () { } // Callback for Modal close
                });
                reference.show = false;
            });
        } else {
            this.router.navigate(['']);
        }
    }
    ChatComponent.prototype.sendMessage = function (data) {
        this.resFlag = true;
        var reference = this;
        if (data.value !== '' && data.value !== undefined) {
            var user = JSON.parse(localStorage.getItem('profile'));
            var obj = {
                message: data.value,
                room: user.room,
                email: user.email,
                login: user.name
            };
            reference.mes.push(obj);
            globalVars.socket.emit("chatMessageToSocketServer", obj, function (respMsg) { });
            $("#first_name2").val(" ");
        } else {
            Materialize.toast('empty message', 4000);
        }
    };

    ChatComponent.prototype.deleteUser = function (email) {
        console.log(email);
        var resp = this;

        globalVars.socket.emit('deleteUser', email, function (bool) {
            Materialize.toast('delete ' + email, 4000);
            for (var i = 0; i < resp.clientsNameList.length; i++) {
                if (resp.clientsNameList[i].email === email) {
                    resp.clientsNameList.splice(i, 1);
                }
            }

        })
    }
    ChatComponent.prototype.sendMessageOnEnter = function ($event, messagebox) {
        if ($event.which === 13) {
            this.sendMessage(messagebox);
        }
    };
    ChatComponent.prototype.update = function () {
        this.resFlag = false;
        this.newUser = false;
        this.exitedUser = false;
    };
    ChatComponent.prototype.toDashBoard = function ($event) {
        $event.preventDefault();
        this.router.navigate(['']);
    };
    ChatComponent.prototype.openModal = function ($event) {
        $event.preventDefault();
        $('#modal1').modal('open');
    };
    ChatComponent.prototype.openWindow = function ($event) {
        $event.preventDefault();
    }
    ChatComponent.prototype.closeModal = function ($event) {
        $event.preventDefault();
        $('#modal1').modal('close');
    };
    ChatComponent.prototype.sendEmail = function (emailTo, emailFrom, Passwordemail) {
        var emailFrom = $('#emailFrom').val();
        var emailTo = $('#emailTo').val();
        var Passwordemail = $('#Passwordemail').val();
        var obj = {
            emailTo: emailTo,
            room: this.currentRoom
        };
        globalVars.socket.emit('sendEmail', obj, function (res) { });
        Materialize.toast('send', 4000);
        $('#modal1').modal('close');
    };
    ChatComponent.prototype.toProgress = function ($event) {
        $event.preventDefault();
        this.router.navigate(['dashboard']);
    };
    ChatComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: "chat-page",
            templateUrl: "./chat.component.html",
            styleUrls: ['css/chat.css']
        }),
        __param(0, core_2.Inject(router_1.Router)),
        __metadata('design:paramtypes', [router_1.Router])
    ], ChatComponent);
    return ChatComponent;
} ());
exports.ChatComponent = ChatComponent;
