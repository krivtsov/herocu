"use strict";
var router_1 = require("@angular/router");
var index_1 = require("./nickName-component/index");
var index_2 = require("./chat-component/index");
var index_3 = require('./registraton/index');
var index_4 = require("./dashBoard/index");
exports.appRoutes = index_1.nickNameRoutes.concat(index_2.chatComponentRoutes, index_3.registerRoutes, index_4.dashboardComponentRoutes);
exports.appRoutingProviders = [router_1.provideRouter(exports.appRoutes)];
exports.routing = router_1.RouterModule.forRoot(exports.appRoutes);
