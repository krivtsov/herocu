import { Routes, RouterModule, provideRouter } from "@angular/router";

import { nickNameRoutes }  from "./nickName-component/index";

import { chatComponentRoutes }  from "./chat-component/index";

import {ModuleWithProviders} from "@angular/core";

import {registerRoutes} from './registraton/index';
import { dashboardComponentRoutes }  from "./dashBoard/index";

export const appRoutes: Routes = [
    ...nickNameRoutes,
    ...chatComponentRoutes,
    ...registerRoutes,
    ...dashboardComponentRoutes
];

export const appRoutingProviders: any[] = [ provideRouter(appRoutes) ];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
