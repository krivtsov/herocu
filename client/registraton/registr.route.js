"use strict";
var registr_component_1 = require("./registr.component");
exports.registerRoutes = [
    { path: 'registration', component: registr_component_1.RegistrComponent }
];
