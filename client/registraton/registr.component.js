"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var globalVars = require("../service/global");
var core_2 = require("@angular/core");
var RegistrComponent = (function () {
    function RegistrComponent(router) {
        this.reg = false;
        this.reqCredit = false;
        this.login = null;
        this.email = null;
        this.password = null;
        this.isTrue = false;
        this.roomName = null;
        this.router = router;
        var reference = this;
        this.cardNumber = 0;
        this.expMonth = 0;
        this.expYear = 0;
        this.cvc = 0;
        this.isValid = true;
        this.show = true;
        this.reg_name = /^([a-zA-Z]){3,40}$/i;
        this.reg_card = /^([0-9]){16}/i;
        this.reg_mm = /^([0-9]){2}/i;
        this.reg_yy = /^([0-9]){2}/i;
        this.reg_cvc = /^([0-9]){3}/i;
        this.reg_email = /^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i;
        globalVars.socket = io('sheltered-springs-88229.herokuapp.com');
        reference.show = false;
    }

    RegistrComponent.prototype.ngOnInit = function () {
        console.log('init req');
        var reference = this;
        globalVars.socket.on("updateUsers", function (val) {
            reference.count = val;
            console.log(val);
        });

        globalVars.socket.emit('joinToChat', {}, function (obj) {
            console.log("obj ", obj === "");
            if (obj !== "") {
                $('#email').val(obj.email);
                $('#room').val(obj.room);
                $('#email_label').hide();
                $('#room_label').hide();
                reference.dis = true;
            }
            console.log(obj);

        });
    }
    RegistrComponent.prototype.submit = function (dataLogin, dataEmail, dataPassword, dataRoomName) {
        this.isValid = true;
        $('#error').text('');
        if (this.reqCredit) {
            console.log('pro user');
            this.login = $('#icon_prefix').val();
            this.email = $('#email').val();
            this.password = $('#password').val();
            this.roomName = $('#room').val();
            this.cardNumber = $('#card').val();
            this.expMonth = $('#mm').val();
            this.expYear = $('#yy').val();
            this.cvc = $('#cvc').val();
            if (this.login == '') {
                this.isValid = false;
                $('#login_label').text('* login');
            } else if (this.reg_name.test(this.login) === false) {
                this.isValid = false;
                $('#error').text('Name format is incorrect.');
            }
            if (this.email === '') {
                this.isValid = false;
                $('#email_label').text('* Email');
            } else if (this.reg_email.test(this.email) === false) {
                this.isValid = false;
                $('#error').text('Email format is incorrect.');
            }
            if (this.password === '') {
                this.isValid = false;
                $('#password_label').text('* password');
            }
            if (this.cardNumber === '') {
                this.isValid = false;
                $('#card_label').text('* Card Number');
            } else if (this.reg_card.test(this.cardNumber) === false) {
                this.isValid = false;
                $('#error').text('Card number format is incorrect.');
            }
            if (this.expMonth === '') {
                this.isValid = false;
                $('#mm_label').text('* Expiration (MM)');
            }
            if (this.expYear === '') {
                this.isValid = false;
                $('#yy_label').text('* Expiration (MM)');
            }
            if (this.cvc === '') {
                this.isValid = false;
                $('#cvc_label').text('* Expiration (MM)');
            }
            if (this.isValid) {
                var obj = {
                    number: this.cardNumber,
                    exp_month: this.expMonth,
                    exp_year: this.expYear,
                    cvc: this.cvc
                }
                Stripe.card.createToken(obj, stripeResponseHandler);
                console.log(obj);
                var resp = this;
                function stripeResponseHandler(status, response) {
                    if (response.error) {
                        console.log(response.error);
                        $('#error').text('card number is incorrect');
                    } else {
                        console.log('token created');
                        obj = {
                            number: resp.cardNumber,
                            exp_month: resp.expMonth,
                            exp_year: resp.expYear,
                            cvc: resp.cvc,
                            token: response.id
                        }
                        globalVars.socket.emit('stripe', obj, function (data) { });
                        var obj;
                        if (resp.login && resp.password && resp.email && resp.roomName) {
                            obj = {
                                name: resp.login,
                                email: resp.email,
                                password: resp.password,
                                room: resp.roomName,
                                pro_user: true
                            };
                        }
                        if (resp.login && resp.password && resp.email && resp.roomName) {
                            globalVars.socket.emit("registration", obj, function (msg, username) {
                                if (msg != 'true') {
                                    resp.mesg = msg + username;
                                    console.log('msg', msg);
                                    $('#error').text(msg + username);
                                }
                                else if (msg == 'true') {
                                    $('#modal1').modal('close');
                                    resp.router.navigate(['']);
                                    localStorage.setItem('profile', JSON.stringify(obj));
                                }
                            });
                        }
                    }
                }
            } else {
                if ($('#error').text() === '')
                    $('#error').text('* Please fill blank fields.');
            }
        } else {
            var reference = this;
            console.log('simple user');
            this.login = $('#icon_prefix').val();
            this.email = $('#email').val();
            this.password = $('#password').val();
            this.room = $('#room').val();
            this.isValid = true;
            $('#error').text('');
            if (this.login == '') {
                this.isValid = false;
                $('#login_label').text('* login');
            } else if (this.reg_name.test(this.login) === false) {
                this.isValid = false;
                $('#error').text('Name format is incorrect.');
            }
            if (this.email === '') {
                this.isValid = false;
                $('#email_label').text('* Email');
            } else if (this.reg_email.test(this.email) === false) {
                this.isValid = false;
                $('#error').text('Email format is incorrect.');
            }
            if (this.password === '') {
                this.isValid = false;
                $('#password_label').text('* password');
            }
            if (this.room !== '') {
                // this.isValid = false;
                globalVars.socket.emit('chatRooms', this.room, function (data) {
                    console.log(data);

                    if (data !== true) {
                        reference.isValid = false;
                        $('#error').text('this room does not exist');
                    } else {
                        console.log(reference.isValid);
                        reference.isValid = true;
                    }

                })
                // globalVars.socket.on('roomIsExist', function (data) {
                // })
            } else {
                this.room = 'noRoom';
            }
                var resp = this;
            setTimeout((function () {
                console.log(resp.isValid);
                if (resp.isValid) {
                    // var room;
                    // if (this.login && this.password && this.email && this.room) {
                    //     room = this.room;
                    // } else {
                    //     room = 'noRoom';
                    // }
                    var obj = {
                        name: resp.login,
                        email: resp.email,
                        password: resp.password,
                        room: resp.room,
                        pro_user: false
                    };
                    console.log(obj);
                    globalVars.socket.emit("registration", obj, function (msg, username) {
                        if (msg != 'true') {
                            this.mesg = msg + username;
                            console.log('msg', msg);
                            $('#error').text(msg + username);
                        }
                        else if (msg == 'true') {
                            $('#modal1').modal('close');
                            resp.router.navigate(['']);
                            console.log('modal close');
                            localStorage.setItem('profile', JSON.stringify(obj));
                        }
                    });
                } else {
                    if ($('#error').text() === '')
                        $('#error').text('* Please fill blank fields.');
                }

            }), 500);
        }
    };
    RegistrComponent.prototype.registr = function ($event) {
        $event.preventDefault();
        this.router.navigate(['registration']);
    };
    RegistrComponent.prototype.addNickname = function ($event, nickname) {
        if ($event.which === 13) {
            this.submit(nickname);
        }
    };
    RegistrComponent.prototype.openModal = function ($event) {
        $('.modal').modal({
            dismissible: true,
            opacity: .5,
            in_duration: 600,
            out_duration: 200,
            starting_top: '0%',
            ending_top: '10%'
        });
        $('#modal1').modal('open');
    };
    RegistrComponent.prototype.closeModal = function ($event) {
        $event.preventDefault();
        $('#modal1').modal('close');
    };
    RegistrComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'registr',
            templateUrl: 'template/registr.component.html',
            styleUrls: ['css/registr.css']
        }),
        __param(0, core_2.Inject(router_1.Router)),
        __metadata('design:paramtypes', [router_1.Router])
    ], RegistrComponent);
    return RegistrComponent;
} ());
exports.RegistrComponent = RegistrComponent;
