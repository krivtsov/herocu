import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import * as globalVars from "../service/global";
import { Inject } from "@angular/core";


@Component({
  moduleId: module.id,
  selector: 'registr',
  templateUrl: 'template/registr.component.html',
  styleUrls: ['css/registr.css']
})

export class RegistrComponent {
  protected reg: boolean = false;
  protected reference: any;
  protected reqCredit: boolean = false;
  protected login: string = null;
  protected email: string = null;
  protected password: string = null;
  protected isTrue: boolean = false;
  protected roomName: string = null;
  protected router;
  protected nav;
  protected cardNumber: number = 0;
  protected expMonth: number = 0;
  protected expYear: number = 0;
  protected cvc: number = 0;
  protected isValid: boolean = true;
  protected show: boolean = true;
  //
  reg_name = /^([a-zA-Z]){3,40}$/i;
  reg_card = /^([0-9]){16}/i;
  reg_mm = /^([0-9]){2}/i;
  reg_yy = /^([0-9]){2}/i;
  reg_cvc = /^([0-9]){3}/i;
  reg_email = /^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i;

  constructor( @Inject(Router) router: Router) {
    this.router = router;
    let reference = this;
    globalVars.socket.on("updateUsers", function (val) {
      reference.count = val;
    });
    this.show = false;
  }

  submit(dataLogin, dataEmail, dataPassword, dataRoomName) {
    globalVars.socket.emit("Room", dataRoomName, function (msg, fn) {

    })
    this.isValid = true;
    if (this.reqCredit) {
      this.login = $('#icon_prefix').val();
      this.email = $('#email').val();
      this.password = $('#password').val();
      this.roomName = $('#room').val();
      this.cardNumber = $('#card').val();
      this.expMonth = $('#mm').val();
      this.expYear = $('#yy').val();
      this.cvc = $('#cvc').val();
      console.log(this.email === '');
      if (this.login == '') {
        this.isValid = false;
        $('#login_label').text('* login');
      } else if (this.reg_name.test(this.login) === false) {
        this.isValid = false;
        $('#error').text('Name format is incorrect.');
      }
      if (this.email === '') {
        this.isValid = false;
        $('#email_label').text('* Email');
      } else if (this.reg_email.test(this.email) === false) {
        this.isValid = false;
        $('#error').text('Email format is incorrect.');
      }
      if (this.password === '') {
        this.isValid = false;
        $('#password_label').text('* password');
      }
      if (this.cardNumber === '') {
        this.isValid = false;
        $('#card_label').text('* Card Number');
      } else if (this.reg_card.test(this.cardNumber) === false) {
        this.isValid = false;
        $('#error').text('Card number format is incorrect.');
      }
      if (this.expMonth === '') {
        this.isValid = false;
        $('#mm_label').text('* Expiration (MM)');
      }
      if (this.expYear === '') {
        this.isValid = false;
        $('#yy_label').text('* Expiration (MM)');
      }
      if (this.cvc === '') {
        this.isValid = false;
        $('#cvc_label').text('* Expiration (MM)');
      }
      if (this.isValid) {
        var obj = {
          number: this.cardNumber,
          exp_month: this.expMonth,
          exp_year: this.expYear,
          cvc: this.cvc
        }
        Stripe.card.createToken(obj, stripeResponseHandler);
        console.log(obj);

        var resp = this;
        function stripeResponseHandler(status, response) {
          if (response.error) {
            console.log(response.error);
            console.log('error shtripe');
          } else {
            console.log('token created');
            obj = {
              number: resp.cardNumber,
              exp_month: resp.expMonth,
              exp_year: resp.expYear,
              cvc: resp.cvc,
              token: response.id
            }
            globalVars.socket.emit('stripe', obj, function (data) {});
            var reference = this;
            var obj;
            if (resp.login && resp.password && resp.email && resp.roomName) {
              obj = {
                name: resp.login, email: resp.email, password: resp.password,room: resp.roomName
              };
            }
            if (resp.login && resp.password && resp.email && resp.roomName) {
              globalVars.socket = io({ query: "userName=" + resp.login });
              globalVars.socket.emit("registration", obj, function (msg, username) {
                if (msg != 'true') {
                  resp.mesg = msg + username;
                  console.log('msg', msg);
                  $('#error').text(msg + username);
                }
                else if (msg == 'true') {
                  $('#modal1').modal('close');
                  resp.router.navigate(['']);
                  localStorage.setItem('profile', JSON.stringify(obj));
                }
              });
            }
          }
        }

      } else {
        if ($('#error').val() === '')
          $('#error').text('* Please fill blank fields.');
      }
    } else {
      this.login = $('#icon_prefix').val();
      this.email = $('#email').val();
      this.password = $('#password').val();
      this.roomName = $('#room').val();

      if (this.login == '') {
        this.isValid = false;
        $('#login_label').text('* login');
      } else if (this.reg_name.test(this.login) === false) {
        this.isValid = false;
        $('#error').text('Name format is incorrect.');
      }
      if (this.email === '') {
        this.isValid = false;
        $('#email_label').text('* Email');
      } else if (this.reg_email.test(this.email) === false) {
        this.isValid = false;
        $('#error').text('Email format is incorrect.');
      }
      if (this.password === '') {
        this.isValid = false;
        $('#password_label').text('* password');
      }
      var resp = this;
      if (this.isValid) {
        if (this.login && this.password && this.email && this.roomName) {
          obj = {
            name: this.login, email: this.email,password: this.password, room: this.roomName
          };
        }
        if (this.login && this.password && this.email && this.roomName) {
          globalVars.socket = io({ query: "userName=" + this.login });
          globalVars.socket.emit("registration", obj, function (msg, username) {
            if (msg != 'true') {
              this.mesg = msg + username;
              $('#error').text(msg + username);
            }
            else if (msg == 'true') {
              $('#modal1').modal('close');
              resp.router.navigate(['']);
              localStorage.setItem('profile', JSON.stringify(obj));
            }
          });
        }
      }
    }
  }

  registr($event) {
    $event.preventDefault();
    this.router.navigate(['registration']);
  }

  addNickname($event, nickname) {
    if ($event.which === 13) { // ENTER_KEY
      this.submit(nickname);
    }
  }

  public openModal($event) {
    $('.modal').modal({
      dismissible: true,
      opacity: .5,
      in_duration: 600,
      out_duration: 200,
      starting_top: '0%',
      ending_top: '10%'
    });
    $('#modal1').show();
  }
  public closeModal($event) {
    $event.preventDefault();
    $('#modal1').hide();
  }
}
