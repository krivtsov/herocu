"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
__export(require("./registr.component"));
__export(require("./registr.route"));
__export(require("./registr.module"));
