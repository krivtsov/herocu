import { NgModule }      from "@angular/core";
import {RouterModule}	from "@angular/router";
import { CommonModule } from "@angular/common";
import { RegistrComponent }  from "./registr.component";

@NgModule({
    imports:      [ CommonModule, RouterModule],
    declarations: [ RegistrComponent ],
    exports: 	  [ RegistrComponent ]
})

export class RegitrNameModule {}