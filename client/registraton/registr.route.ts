import {Router, RouterConfig, Routes} from "@angular/router";

import {RegistrComponent} from "./registr.component";

import * as myGlobal from "../service/global";

export const registerRoutes: Routes = [
    { path: 'registration', component: RegistrComponent }
];