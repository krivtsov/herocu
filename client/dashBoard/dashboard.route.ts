import { Router, Routes } from "@angular/router";

import { dashboardComponent } from "./dashboard.component";

import * as myGlobal from "../service/global";

export const dashboardComponentRoutes: Routes = [
    { path: 'dashboard', component: dashboardComponent }
];