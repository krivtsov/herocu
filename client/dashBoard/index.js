"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
__export(require("./dashboard.component"));
__export(require("./dashboard.route"));
__export(require("./dashboard.module"));
