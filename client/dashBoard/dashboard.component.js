"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require("@angular/core");
var common_1 = require('@angular/common');
var globalVars = require("../service/global");
var router_1 = require("@angular/router");
var core_2 = require("@angular/core");
var progress_directive_1 = require('./progressBar/progress.directive');
var bar_component_1 = require('./progressBar/bar.component');
var progress_component_1 = require('./progressBar/progress.component');
require("/socket.io/socket.io.js");
var dashboardComponent = (function () {
    function dashboardComponent(router) {
        this.max = 200;
        this.timerCount = 0;
        this.timerWeek = 0;
        this.timerMonth = 0;
        this.stackedValues = [];
        this.count = 0;
        this.router = router;
        this.but = 'today';
        this.rait = 'today';
        this.tyW = [];
        this.tyM = [];
        this.masT = [];
        this.show = true;
        this.chat_user = false;

        // label
        this.label_today = [];
        this.label_week = [];
        this.label_month = [];
    }
    dashboardComponent.prototype.ngOnInit = function () {
        var reference = this;
        globalVars.socket.on("updateUsers", function (val) {
            reference.count = val;
        });
        globalVars.socket.on('router', function (link) {
            reference.router.navigate([link]);
        });
        if (localStorage.getItem('profile') !== null) {
            var name = JSON.parse(localStorage.getItem('profile'));
            this.auth = true;
            var reference = this;
            globalVars.socket.emit("login_", name, function (msg, username) {
                if (msg != 'true') {
                    this.mesg = msg + username;
                    console.log('msg', msg);
                    $('#change').text(msg + username);
                }
                else if (msg == 'true') {
                    localStorage.clear();
                    localStorage.setItem('profile', JSON.stringify(username));
                    if (username.room !== 'noRoom')
                        reference.chat_user = true;
                    reference.authorizationUser = true;
                    reference.auth = false;
                    reference.currentValue = username.tymerToday;
                }
            });
            globalVars.socket.emit('timerUp', name, function (msg, bool) {
                var countToday = 0;
                var countWeek = 0;
                var countMonth = 0;
                //day
                var cou = 0;
                var twoEt = 0;
                var three = 0;
                var four = 0;
                // week
                var couW = 0;
                var twoEtW = 0;
                var threeW = 0;
                var fourW = 0;
                var fiveW = 0;
                var sixW = 0;
                var sevenW = 0;
                // month
                var couO = 0;
                var couM = 0;
                var twoEtM = 0;
                var threeM = 0;
                var fourM = 0;
                var fiveM = 0;
                var sixM = 0;
                // variable of date
                var date_day_of_month = new Date().getDate();
                var date_hours = new Date().getHours();
                var date_day_of_week = new Date().getDay();
                // data
                reference.masT = [0];
                reference.label_today = ['6', '12', '18', '24'];
                var max = 0;
                if (msg.length === 0) {
                    reference.masT = [0, 0, 0, 0];
                    reference.tyW = [0, 0, 0, 0, 0, 0, 0];
                    reference.tyM = [0, 0, 0, 0];
                }
                console.log(new Date().getHours() + new Date().getHours() - 24);
                for (var i = 0; i < msg.length; i++) {
                    var date = new Date(msg[i].date);
                    if (date >= new Date(new Date() - 86400000) && date <= new Date()) {
                        countToday++;
                        if (date >= new Date(new Date() - 21600000)) { // 18 >
                            if (date.getHours() >= max) {
                                max = date.getHours();
                                reference.label_today[3] = max.toString();
                            }
                            cou++;
                        } else {
                            reference.label_today[3] = new Date().getHours().toString();
                        }
                        if (date <= new Date(new Date() - 21600000) && date >= new Date(new Date() - 43200000)) {  // 18< and 12>
                            reference.label_today[3] = new Date(new Date() - 43200000).getHours().toString();
                            twoEt++;
                        } else {
                            reference.label_today[2] = new Date(new Date() - 21600000).getHours().toString();
                        }
                        if (date <= new Date(new Date() - 43200000) && date >= new Date(new Date() - 64800000)) { // 12< and 6>
                            reference.label_today[3] = new Date(new Date() - 64800000).getHours().toString();
                            three++;
                        } else {
                            reference.label_today[1] = new Date(new Date() - 43200000).getHours().toString();
                        }
                        if (date <= new Date(new Date() - 64800000) && date >= new Date(new Date() - 86400000)) { // 6>and 0
                            reference.label_today[3] = new Date(new Date() - 86400000).getHours().toString();
                            four++;
                        } else {
                            reference.label_today[0] = new Date(new Date() - 86400000).getHours().toString();
                        }
                    }
                    reference.masT = [four, three, twoEt, cou];

                    // week
                    reference.label_week = ['1', '2', '3', '4', '5', '6', '7'];
                    if (date >= new Date(new Date() - 604800000) && date <= new Date()) {
                        countWeek++;
                        if (date >= new Date(new Date() - 86400000)) {
                            couW++;
                            reference.label_week[6] = date.getDay().toString();
                        } else {
                            reference.label_week[6] = new Date(new Date() - 86400000).getDay().toString();
                        }
                        if (date <= new Date(new Date() - 86400000) && date >= new Date(new Date() - 172800000)) {
                            twoEtW++;
                            reference.label_week[5] = date.getDay().toString();
                        } else {
                            reference.label_week[5] = new Date(new Date() - 172800000).getDay().toString();
                        }
                        if (date <= new Date(new Date() - 172800000) && date >= new Date(new Date() - 259200000)) {
                            threeW++;
                            reference.label_week[4] = date.getDay().toString();
                        } else {
                            reference.label_week[4] = new Date(new Date() - 259200000).getDay().toString();
                        }
                        if (date <= new Date(new Date() - 259200000) && date >= new Date(new Date() - 345600000)) {
                            fourW++;
                            reference.label_week[3] = date.getDay().toString();
                        } else {
                            reference.label_week[3] = new Date(new Date() - 345600000).getDay().toString();
                        }
                        if (date <= new Date(new Date() - 345600000) && date >= new Date(new Date() - 432000000)) {
                            fiveW++;
                            reference.label_week[2] = date.getDay().toString();
                        } else {
                            reference.label_week[2] = new Date(new Date() - 432000000).getDay().toString();
                        }
                        if (date <= new Date(new Date() - 432000000) && date >= new Date(new Date() - 518400000)) {
                            sixW++;
                            reference.label_week[1] = date.getDay().toString();
                        } else {
                            reference.label_week[1] = new Date(new Date() - 518400000).getDay().toString();
                        }

                        if (date <= new Date(new Date() - 518400000) && date >= new Date(new Date() - 604800000)) {
                            sevenW++;
                            reference.label_week[0] = date.getDay().toString();
                        } else {
                            reference.label_week[0] = new Date(new Date() - 604800000).getDay().toString();
                        }
                    }
                    reference.tyW = [sevenW, sixW, fiveW, fourW, threeW, twoEtW, couW];




                    // month
                    reference.label_month = ['','6', '12', '18', '24', '30'];
                    if (date >= new Date(new Date() - 2592000000) && date <= new Date()) {
                        countMonth++;
                        if (date >= new Date(new Date() - 432000000)) {  // 6
                            couO++;
                            reference.label_month[5] = date.getDate().toString();
                        } else {
                            reference.label_month[5] = new Date().getDate().toString();
                        }


                        if (date <= new Date(new Date() - 518400000) && date >= new Date(new Date() - 1036800000)) {  // 12
                            console.log("date month ------->", date);
                            console.log(date.getDate().toString());
                            fiveW++;
                            reference.label_month[4] = new Date(new Date() - 518400000).getDate().toString();
                        }
                        else {
                            reference.label_month[4] = new Date(new Date() - 518400000).getDate().toString();
                        }


                        if (date <= new Date(new Date() - 518400000) && date >= new Date(new Date() - 1036800000)) {  // 12
                            console.log("date month ------->", date);
                            console.log(date.getDate().toString());
                            couM++;
                            reference.label_month[3] = new Date(new Date() - 1036800000).getDate().toString();
                        }
                        else {
                            reference.label_month[3] = new Date(new Date() - 1036800000).getDate().toString();
                        }


                        if (date <= new Date(new Date() - 1036800000) && date >= new Date(new Date() - 1555200000)) {  // 15
                            twoEtM++;
                            reference.label_month[2] = new Date(new Date() - 1555200000).getDate().toString();
                        } else {
                            reference.label_month[2] = new Date(new Date() - 1555200000).getDate().toString();
                        }


                        if (date <= new Date(new Date() - 1555200000) && date >= new Date(new Date() - 2073600000)) {  // 10
                            threeM++;
                            reference.label_month[1] = new Date(new Date() - 2073600000).getDate().toString();
                        } else {
                            reference.label_month[1] = new Date(new Date() - 2073600000).getDate().toString();
                        }


                        if (date <= new Date(new Date() - 2073600000) && date >= new Date(new Date() - 2592000000)) {  // 5
                            fourM++;
                            reference.label_month[0] = new Date(new Date() - 2073600000).getDate().toString();
                        } else {
                            reference.label_month[0] = new Date(new Date() - 2592000000).getDate().toString();
                        }

                        // if (date <= new Date(new Date() - 2160000000) && date >= new Date(new Date() - 2592000000)) {
                        //     fiveM++;
                        //     reference.label_month[0] = date.getDate().toString();
                        // } else {
                        //     reference.label_month[0] = new Date(new Date() - 2160000000).getDate().toString();
                        // }
                    }
                    reference.tyM = [fourM, threeM, twoEtM,couM, fiveW, couO];
                }
                reference.timerCount = countToday;
                reference.timerWeek = countWeek;
                reference.timerMonth = countMonth;
                reference.show = false;
            });
            globalVars.socket.emit('updateListUsers', name, function (msg, bool) {
                reference.raiting('today');
                reference.clientsList = msg;
            });
        } else
            this.router.navigate(['']);

    }
    dashboardComponent.prototype.ngAfterViewInit = function () { };
    dashboardComponent.prototype.toDashBoard = function ($event) {
        $event.preventDefault();
        this.router.navigate(['']);
    };
    dashboardComponent.prototype.raiting = function (text) {
        if (text === 'week') {
            var data = {
                labels: this.label_week,
                series: [
                    this.tyW
                ]
            };
        } else if (text === 'month') {
            var data = {
                labels: this.label_month,
                series: [
                    this.tyM
                ]
            };
        } else if (text === 'today') {
            var data = {
                labels: this.label_today,
                series: [
                    this.masT
                ]
            };
        }
        new Chartist.Line('.ct-chart', data, {
            stretch: true,
            fullWidth: true,
            chartPadding: {
                right: 40
            }
        });
    }
    dashboardComponent.prototype.toChat = function ($event) {
        $event.preventDefault();
        this.router.navigate(['chat']);
    };
    __decorate([
        core_1.ViewChild("chessCanvas"),
        __metadata('design:type', core_1.ElementRef)
    ], dashboardComponent.prototype, "chessCanvas", void 0);
    dashboardComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: "dashboard-page",
            templateUrl: "template/dashboard.component.html",
            styleUrls: ['css/dashboard.css'],
            directives: [progress_directive_1.Progress, bar_component_1.Bar, progress_component_1.Progressbar, common_1.CORE_DIRECTIVES],
        }),
        __param(0, core_2.Inject(router_1.Router)),
        __metadata('design:paramtypes', [router_1.Router])
    ], dashboardComponent);
    return dashboardComponent;
} ());
exports.dashboardComponent = dashboardComponent;
