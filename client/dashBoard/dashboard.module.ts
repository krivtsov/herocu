import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { dashboardComponent } from "./dashboard.component";

@NgModule({
    imports: [CommonModule],
    declarations: [dashboardComponent],
    exports: [dashboardComponent]
})

export class dashboardModule { }