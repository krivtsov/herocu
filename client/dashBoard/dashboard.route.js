"use strict";
var dashboard_component_1 = require("./dashboard.component");
exports.dashboardComponentRoutes = [
    { path: 'dashboard', component: dashboard_component_1.dashboardComponent }
];
