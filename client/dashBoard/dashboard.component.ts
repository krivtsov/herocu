import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { CORE_DIRECTIVES } from '@angular/common';
import * as globalVars from "../service/global";
import { Router } from "@angular/router";
import { Inject } from "@angular/core";
import { Progress } from './progressBar/progress.directive';
import { Bar } from './progressBar/bar.component';
import { Progressbar } from './progressBar/progress.component';

import "/socket.io/socket.io.js";

@Component({
    moduleId: module.id,
    selector: "dashboard-page",
    templateUrl: "template/dashboard.component.html",
    styleUrls: ['css/dashboard.css'],
    directives: [Progress, Bar, Progressbar, CORE_DIRECTIVES],
})
export class dashboardComponent {
    public max: number = 200;
    public currentValue: number;
    public type: string;
    public timerCount: number = 0;
    public timerWeek: number = 0;
    protected router;
    public rait;
    public timerMonth = 0;
    public tyW: any[] = [];
    public tyM: any[] = [];
    public masT: any[] = [];
    public reference = this;
    public stackedValues: any[] = [];
    public show: boolean = true;
    ngAfterViewInit() {
    }
    count: number = 0;
    clientsList: string[];
    constructor( @Inject(Router) router: Router) {
        this.router = router;
        let reference = this;
        globalVars.socket.on("updateUsers", function (val) {
            reference.count = val;
        })
        globalVars.socket.on('router', function (link) {
            reference.router.navigate([link]);
        });

        if (localStorage.getItem('profile') !== null) {
            var name = JSON.parse(localStorage.getItem('profile'));
            globalVars.socket = io({ query: "userName=" + this.email });
            console.log(name);
            globalVars.socket.emit("login_", JSON.parse(name), function (msg, username) {
                if (msg != 'true') {
                    reference.router.navigate(['']);
                }
            });
            globalVars.socket.emit('timerUp', name, function (msg, bool) {
                var countToday = 0;
                var countWeek = 0;
                var countMonth = 0;
                var cou = 0;
                var twoEt = 0;
                var three = 0;
                var four = 0;
                var couW = 0;
                var twoEtW = 0;
                var threeW = 0;
                var fourW = 0;
                var fiveW = 0;
                var sixW = 0;
                var sevenW = 0;
                var couM = 0;
                var twoEtM = 0;
                var threeM = 0;
                var fourM = 0;
                if (msg.length === 0) {
                    reference.masT = [cou, twoEt, three, four];
                    reference.tyW = [couW, twoEtW, threeW, fourW, fiveW, sixW, sevenW];
                    reference.tyM = [couM, twoEtM, threeM, fourM];
                }
                for (var i = 0; i < msg.length; i++) {
                    var date = new Date(msg[i].date);
                    if (date.getDate() === new Date().getDate()) {
                        countToday++;
                        if (date.getHours() <= 6) {
                            cou++;
                        }
                        if (12 >= date.getHours() && date.getHours() >= 6) {
                            twoEt++;
                        }
                        if (18 >= date.getHours() && date.getHours() >= 12) {
                            three++;
                        }
                        if (18 >= date.getHours() && date.getHours() >= 24) {
                            four++;
                        }
                        reference.masT = [cou, twoEt, three, four];
                    }
                    if (date.getDate() >= new Date().getDate() - 7) {
                        countWeek++;
                        console.log(date.getDay());
                        if (date.getDay() <= 1) {
                            couW++;
                        }
                        if (date.getDay() === 1) {
                            twoEtW++;
                        }
                        if (date.getDay() === 2) {
                            threeW++;
                        }
                        if (date.getDay() === 3) {
                            fourW++;
                        }
                        if (date.getDay() === 4) {
                            fiveW++;
                        }
                        if (date.getDay() === 5) {
                            sixW++;
                        }
                        if (date.getDay() === 6) {
                            sevenW++;
                        }
                        reference.tyW = [couW, twoEtW, threeW, fourW, fiveW, sixW, sevenW];
                    }
                    if (date.getMonth() === new Date().getMonth()) {
                        countMonth++;
                        if (date.getDate() <= 6) {
                            couM++;
                        }
                        if (12 >= date.getDate() && date.getDate() >= 6) {
                            twoEtM++;
                        }
                        if (18 >= date.getDate() && date.getDate() >= 12) {
                            threeM++;
                        }
                        if (18 >= date.getDate() && date.getDate() >= 24) {
                            fourM++;
                        }
                        reference.tyM = [couM, twoEtM, threeM, fourM];
                    }
                }
                var data = {
                    labels: ['6', '12', '18', '24'],
                    series: [
                        reference.masT
                    ]
                };
                console.log(data.series);
                var options = {
                    width: 700,
                    height: 300
                };

                reference.timerCount = countToday;
                reference.timerWeek = countWeek;
                reference.timerMonth = countMonth;

                var obj = {
                    timerToday: reference.timerCount,
                    timerWeek: reference.timerWeek,
                    timerMonth: reference.timerMonth,
                    email: name.email
                }
                globalVars.socket.emit('UpdateTimer', obj, function (msg, name) {
                    console.log(msg);
                });
            });
            globalVars.socket.emit('updateListUsers', name, function (msg, bool) {
                reference.currentValue = msg.timerToday;
            });
            globalVars.socket.on('updateSocketList', function (list) {
                reference.clientsList = list;
                reference.show = false;
            });
        } else {
            this.router.navigate(['']);
        }
        this.generateNewProgressValues();
        this.generateStackedValues();
    }
    private generateNewProgressValues() {
        let value = Math.floor((Math.random() * 10) + 1);
        let type: string;

        if (value < 20) {
            type = 'success';
        } else if (value < 40) {
            type = 'info';
        } else if (value < 60) {
            type = 'warning';
        } else {
            type = 'danger';
        }
        this.currentValue = value;
        this.type = type;
    };
    private generateStackedValues() {
        let types = ['success', 'info', 'warning', 'danger'];
        this.stackedValues = [];
        let total = 0;
        for (let i = 0, n = Math.floor((Math.random() * 4) + 1); i < n; i++) {
            let index = Math.floor((Math.random() * 4));
            let value = Math.floor((Math.random() * 30) + 1);
            total += value;
            this.stackedValues.push({
                value: value,
                max: value,
                type: types[index]
            });
        }
    };
    toDashBoard($event) {
        $event.preventDefault();
        this.router.navigate(['']);
    }
    toChat($event) {
        $event.preventDefault();
        this.router.navigate(['chat']);
    }
    public raiting(text) {
        if (text === 'week') {
            var data = {
                // A labels array that can contain any sort of values
                labels: ['1', '2', '3', '4', '5', '6', '7'],
                // Our series array that contains series objects or in this case series data arrays
                series: [
                    this.tyW
                ]
            };
        } else if (text === 'month') {
            var data = {
                // A labels array that can contain any sort of values
                labels: ['6', '12', '18', '24'],
                // Our series array that contains series objects or in this case series data arrays
                series: [
                    this.tyM
                ]
            };
        } else if (text === 'today') {
            var data = {
                // A labels array that can contain any sort of values
                labels: ['6', '12', '18', '24'],
                // Our series array that contains series objects or in this case series data arrays
                series: [
                    this.masT
                ]
            };
        }
        var options = {
            width: 700,
            height: 300
        };
        new Chartist.Line('.ct-chart', data, options);
    }
}