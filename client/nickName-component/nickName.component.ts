import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import * as globalVars from "../service/global";
import { Inject } from "@angular/core";
import { AuthService } from './nickName.service';


/// <reference path="../../typings/globals/jquery/index.d.ts/>



import "/socket.io/socket.io.js";


@Component({
    moduleId: module.id,
    selector: "nick-name",
    templateUrl: "nickName.component.html",
    styleUrls: ['nick.css'],
    providers: [AuthService]
})

export class NickNameComponent {
    reference: any;
    audio;
    count: number = 0;
    event: boolean = false;
    second: number = 0;
    minute: number = 25;
    protected t;
    time: number = 0;
    login: string = null;
    email: string = null;
    password: string = null;
    isTrue: boolean = false;
    auth: boolean = false;
    authorizationUser: boolean = false;
    protected router;
    protected nav;
    roomName: boolean = false;
    nameRoom: string = null;
    timerCount: number = 0;
    timerWeek: number = 0;
    timerMonth: number = 0
    protected show: boolean = true;
    tim: boolean = false;


    constructor( @Inject(Router) router: Router, private autho: AuthService) {
        this.router = router;
        let reference = this;
        globalVars.socket = io({ query: "userName=" + 'connect' });
        globalVars.socket.on("updateUsers", function (val) {
            reference.count = val;
            console.log(val);
            $('.modal').modal({
                dismissible: true,
                opacity: .5,
                in_duration: 600,
                out_duration: 500,
                starting_top: '10%',
                ending_top: '4%',
                ready: function (modal, trigger) {
                    console.log(modal, trigger);
                },
                complete: function () { } // Callback for Modal close
            });
        });

        if (localStorage.getItem('profile') !== null) {
            globalVars.socket.on('router', function (link) {
                reference.router.navigate([link]);
            });
            var name = localStorage.getItem('profile');
            var obj = JSON.parse(name);
            globalVars.socket = io({ query: "userName=" + this.email });
            console.log(name);
            this.auth = true;
            globalVars.socket.emit('updateListUsers', JSON.parse(name), function () {

            })
            globalVars.socket.emit('timerUp', JSON.parse(name), function (msg, bool) {
                console.log(msg);
                var countToday = 0;
                var countWeek = 0;
                var countMonth = 0;
                for (var i = 0; i < msg.length; i++) {
                    var date = new Date(msg[i].date)
                    if (date.getDate() === new Date().getDate()) {
                        countToday++;
                    }
                    if (date.getDate() >= new Date().getDate() - 7) {
                        countWeek++;
                    }
                    if (date.getMonth() === new Date().getMonth()) {
                        countMonth++;
                    }
                }
                reference.timerCount = countToday;
                reference.timerWeek = countWeek;
                reference.timerMonth = countMonth;

                var obj = {
                    timerToday: reference.timerCount,
                    timerWeek: reference.timerWeek,
                    timerMonth: reference.timerMonth,
                    email: JSON.parse(name).email
                }
                globalVars.socket.emit('UpdateTimer', obj, function (msg, name) {
                    console.log(msg);
                });
            });
            globalVars.socket.emit("login_", JSON.parse(name), function (msg, username) {
                if (msg != 'true') {
                    this.mesg = msg + username;
                    console.log('msg', msg);
                    localStorage.clear();
                    $('#change').text(msg + username);
                }
                else if (msg == 'true') {
                    console.log('ok');
                    // reference.router.navigate(['chat']);
                    reference.authorizationUser = true;
                    reference.auth = false;
                    reference.show = false;
                }
            });
        } else {
            this.show = false;
        }
    }


    public start() {
        this.tim = true;
        if (this.time == 0) {
            this.time = 1;
            this.t = setInterval(() => this.timer(), 50);
        }
    }

    public pause(minute, second) {
        // clearInterval(this.t);
        // this.minute = minute;
        // this.second = second;
        // this.time = 0;
        // this.tim = false;
        swal(
            'You can not pause the Timer!',
            'Keep at it!',
            'success'
        )
    }




    private timer() {
        if (this.minute == 0 && this.second == 0) {
            if (this.event == true) {
                this.audio = new Audio();
                this.audio.src = "nickName-component/sound/timer.mp3";
                setTimeout(this.audio.autoplay = true, 3000);
            }
            clearInterval(this.t);
            this.timerCount++;
            this.timerWeek++;
            this.timerMonth++;
            var name = JSON.parse(localStorage.getItem('profile'));
            var obj = {
                email: name.email,
                timerToday: this.timerCount,
                timerWeek: this.timerWeek,
                timerMonth: this.timerMonth
            };
            var obj1 = {
                email: name.email,
                room: name.room
            };
            globalVars.socket.emit('UpdateTimer', obj, function (msg, name) {
            });
            globalVars.socket.emit('timerTo', obj1, function (msg, name) {
            });

            clearInterval(this.t);
            this.minute = 25;
            this.second = 0;
            this.time = 0;
            this.tim = false;
            //return this.stop();
        }
        if (this.second == 0) {
            this.minute--;
            this.second = 59;
        }
        else {
            this.second--;
        }


    }
    public stop() {
        var reference = this;
        swal({
            title: 'DUDE?',
            text: "Is your office on fire? No? Keep at it! Always finish your timer!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Stop! I have another emergency!'
        }).then(function () {
            clearInterval(reference.t);
            reference.minute = 25;
            reference.second = 1;
            reference.pause(reference.minute, reference.second);
            reference.tim = false;
        })
    }



    submit(dataEmail, dataPassword) {
        console.log('ok');
        // this.login = dataLogin.value;
        this.email = dataEmail.value;
        this.password = dataPassword.value;
        let reference = this;
        var obj;
        if (this.password && this.email) {
            obj = {
                email: this.email,
                password: this.password
            }
            globalVars.socket = io({ query: "userName=" + this.login });
            globalVars.socket.emit("login_", obj, function (msg, room) {
                if (msg != 'true') {
                    $('#error').text('email or password is incorrect');
                } else if (msg == 'true') {
                    console.log('ok');
                    var obj = {
                        email: reference.email,
                        password: reference.password,
                        room: room
                    }
                    // reference.router.navigates(['chat']);
                    reference.authorizationUser = true;
                    reference.auth = false;
                    localStorage.setItem('profile', JSON.stringify(obj));
                    $('#modal1').modal('close');
                }

            })
            globalVars.socket.emit('timerUp', obj, function (msg, bool) {
                console.log(msg);
                var countToday = 0;
                var countWeek = 0;
                var countMonth = 0;
                for (var i = 0; i < msg.length; i++) {
                    var date = new Date(msg[i].date)
                    if (date.getDate() === new Date().getDate()) {
                        countToday++;
                    }
                    if (date.getDate() >= new Date().getDate() - 7) {
                        countWeek++;
                    }
                    if (date.getMonth() === new Date().getMonth()) {
                        countMonth++;
                    }
                }
                reference.timerCount = countToday;
                reference.timerWeek = countWeek;
                reference.timerMonth = countMonth;
            });
        }
        //this.autho.login();
    }

    toProgress($event) {
        $event.preventDefault();
        this.router.navigate(['dashboard']);
    }

    toChat($event) {
        $event.preventDefault();
        this.router.navigate(['chat']);
    }

    exit($event) {
        $event.preventDefault();
        localStorage.clear();
    }

    registr($event) {
        $event.preventDefault();
        this.router.navigate(['registration']);
    }

    signIn($event) {
        $event.preventDefault();
        this.auth = true;
    }


    public openModal($event) {
        $event.preventDefault();
        $('#modal1').modal('open');
        this.auth = true;
    }
    public closeModal($event) {
        $event.preventDefault();
        $('#modal1').modal('close');
    }

    // RoomCreated(name){
    //   this.nameRoom = name;
    //   globalVars.socket.emit("CreateRoom", name, function(val){

    //   })
    // }

    // addNickname($event, nickname) {
    //   if ($event.which === 13) { // ENTER_KEY
    //     this.submit(nickname);
    //   }
    // }
}