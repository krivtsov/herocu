"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var globalVars = require("../service/global");
var core_2 = require("@angular/core");
var nickName_service_1 = require('./nickName.service');
/// <reference path="../../typings/globals/jquery/index.d.ts/>
require("/socket.io/socket.io.js");
var NickNameComponent = (function () {
    function NickNameComponent(router, autho) {
        this.autho = autho;
        this.count = 0;
        this.event = false;
        this.second = 0;
        this.minute = 25;
        this.time = 0;
        this.login = null;
        this.email = null;
        this.password = null;
        this.isTrue = false;
        this.auth = false;
        this.authorizationUser = false;
        this.roomName = false;
        this.nameRoom = null;
        this.timerCount = 0;
        this.timerWeek = 0;
        this.timerMonth = 0;
        this.router = router;
        this.show = true;
        this.tim = false;
        this.chat_user = false;
        globalVars.socket = io('sheltered-springs-88229.herokuapp.com');
    }

    // download data from server

    NickNameComponent.prototype.ngOnInit = function () {
        var reference = this;
        globalVars.socket.on("updateUsers", function (val) {
            reference.count = val;
            console.log("update user done ", val);
        });
        globalVars.socket.emit('joinToRoom', {}, function (user) { });
        globalVars.socket.on('router', function (link) {
            reference.router.navigate([link]);
        });
        if (localStorage.getItem('profile') !== null) {
            var name = localStorage.getItem('profile');
            globalVars.socket.emit("login_", JSON.parse(name), function (msg, user) {
                if (msg != 'true') {
                    this.mesg = msg + user;
                    localStorage.clear();
                    reference.show = false;
                    $('#change').text(msg + user);
                }
                else if (msg == 'true') {
                    console.log(user);
                    reference.timerCount = user.tymerToday;
                    reference.timerWeek = user.tymerWeek;
                    reference.timerMonth = user.typerMonth;
                    globalVars.socket.emit("active", JSON.parse(name), function (msg) { });
                    reference.authorizationUser = true;
                    reference.auth = false;
                    reference.show = false;
                    // localStorage.clear();
                    // localStorage.setItem('profile', JSON.stringify(user));
                    console.log('login user done');
                    if (user.room !== 'noRoom')
                        reference.chat_user = true;

                    globalVars.socket.emit('joinToRoom', {}, function (user) {
                        if (user.room !== 'room')
                            reference.chat_user = true;
                        console.log(user);
                        localStorage.clear();
                        var obj = {
                            name: user.name,
                            email: user.email,
                            room: user.room
                        };
                        localStorage.setItem('profile', JSON.stringify(obj))
                    })
                }
            });
            this.auth = true;
        } else
            this.show = false;
    };

    //


    NickNameComponent.prototype.start = function () {
        this.tim = true;
        var _this = this;
        if (this.time === 0) {
            this.time = 1;
            this.t = setInterval(function () { return _this.timer(); }, 0.1);
            this.minute = 25;
            this.second = 0;
        }
    };
    NickNameComponent.prototype.pause = function (minute, second) {
        swal(
            'You can not pause the Timer!',
            'Keep at it!',
            'success'
        )
    };
    NickNameComponent.prototype.timer = function () {
        if (this.minute == 0 && this.second == 0) {
            if (this.event) {
                this.audio = new Audio();
                this.audio.src = "nickName-component/sound/timer_short.mp3";
                this.audio.play();
            }
            clearInterval(this.t);
            this.timerCount++;
            this.timerWeek++;
            this.timerMonth++;
            if (localStorage.getItem('profile') !== null) {
                var name = JSON.parse(localStorage.getItem('profile'));
                var obj1 = {
                    email: name.email,
                    room: name.room
                };
                globalVars.socket.emit('UpdateTimer', name.email, function (msg, name) {
                });
                globalVars.socket.emit('timerTo', obj1, function (msg, name) {
                });
            }
            clearInterval(this.t);
            this.minute = 25;
            this.second = 0;
            this.time = 0;
            this.tim = false;
            return;
        }
        if (this.second == 0) {
            this.minute--;
            this.second = 59;
        }
        else {
            this.second--;
        }
    };
    NickNameComponent.prototype.stop = function () {
        var reference = this;
        swal({
            title: 'DUDE?',
            text: "Is your office on fire? No? Keep at it! Always finish your timer!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Stop! I have another emergency!'
        }).then(function () {
            clearInterval(reference.t);
            reference.minute = 25;
            reference.second = 0;
            reference.time = 0;
            reference.tim = false;
        })
    };
    NickNameComponent.prototype.submit = function (dataEmail, dataPassword) {
        this.email = dataEmail.value;
        this.password = dataPassword.value;
        var reference = this;
        if (this.password && this.email) {
            var obj = { email: this.email, password: this.password };
            globalVars.socket.emit("login_", obj, function (msg, user) {
                if (msg != 'true') {
                    $('#error').text('email or password is incorrect');
                }
                else if (msg == 'true') {
                    var obj1 = {
                        email: reference.email,
                        password: reference.password,
                        room: user.room
                    };
                    reference.authorizationUser = true;
                    reference.auth = false;

                    reference.timerCount = user.tymerToday;
                    reference.timerWeek = user.tymerWeek;
                    reference.timerMonth = user.typerMonth;

                    localStorage.setItem('profile', JSON.stringify(obj1));
                    if (user.room !== 'noRoom')
                        reference.chat_user = true;
                    $('#modal1').modal('close');
                }
            });
        }
    };
    NickNameComponent.prototype.check = function () {
        if (this.event)
            this.event = false;
        else
            this.event = true;
    }
    NickNameComponent.prototype.exit = function ($event) {
        $event.preventDefault();
        this.authorizationUser = false;
        this.chat_user = false;
        localStorage.clear();
        this.timerCount = 0;
        this.timerWeek = 0;
    }
    NickNameComponent.prototype.signIn = function ($event) {
        $event.preventDefault();
        this.auth = true;
    };
    NickNameComponent.prototype.openModal = function ($event) {
        $('.modal').modal({
            dismissible: true,
            opacity: .5,
            in_duration: 600,
            out_duration: 500,
            starting_top: '10%',
            ending_top: '4%',
            ready: function (modal, trigger) {
            },
            complete: function () { } // Callback for Modal close
        });
        $event.preventDefault();
        $('#modal1').modal('open');
        this.auth = true;
    };
    NickNameComponent.prototype.closeModal = function ($event) {
        $event.preventDefault();
        $('#modal1').modal('close');
    };
    NickNameComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: "nick-name",
            templateUrl: "nickName.component.html",
            styleUrls: ['nick.css'],
            providers: [nickName_service_1.AuthService]
        }),
        __param(0, core_2.Inject(router_1.Router)),
        __metadata('design:paramtypes', [router_1.Router, nickName_service_1.AuthService])
    ], NickNameComponent);
    return NickNameComponent;
} ());
exports.NickNameComponent = NickNameComponent;
