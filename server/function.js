var crypto = require('crypto');
var md5sum = crypto.createHash('md5');

exports.guestNumber = 1;
exports.nickNames = {};
exports.namesUsed = [];
exports.currentRoom = {};
exports.user_email;
exports.hash = function(text) {
	return crypto.createHash('sha1')
	.update(text).digest('base64')
}

exports.joinRoom =  function (socket, room) {
	socket.join(room);
	exports.currentRoom[socket.id] = room;
	socket.emit('joinResult', { room: room });
	socket.broadcast.to(room).emit('message', {
		text: exports.nickNames[socket.id] + 'has joined' + room + '.'
	});

}
