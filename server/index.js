"use strict";
let express = require("express");
var path = require('path');
var crypto = require('crypto');
var md5sum = crypto.createHash('md5');
let chat_app = require('express')();
let http = require('http').Server(chat_app);
let io = require('socket.io')(http);
var mongoose = require('mongoose');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
let clientListNames = [];
var MongoClient = require('mongodb').MongoClient;
var count = 0;
var db = mongoose.connection;
var router = express.Router();
var mailOptions, nodemailer, transporter;
var nodemailer = require('nodemailer');
var sendMailTransport = require('nodemailer-sendmail-transport');
var stripe = require("stripe")("sk_test_NY1jtd0SiY355n3nt7bTgpPD");
var url = require("url");
var url_d = '';

var api_key = 'key-e269879e4a42a0901e648202c75519e9';
var domain = 'sandbox645f4e21772140478452961ec69bca68.mailgun.org';
var Mailgun = require('mailgun-js');

var api = require('./function.js');
var model = require('./modelDB.js');
var gravatar = require('gravatar');

chat_app.use(express.static(__dirname + '/../root'));
chat_app.use(express.static(__dirname + '/../client/'));
chat_app.use(express.static(__dirname + '/../node_modules'));
chat_app.use(express.static(__dirname + '/../bower_components'));

var url_param = '';
var chat_param = '';



chat_app.get('/', function (req, res) {
	url_d = req.url;
});

chat_app.get('/registration/:email/:room', function (req, res) {
	var mas = req.url.split('/');
	console.log(req.params);
	console.log(mas);
	console.log('post');
	url_d = mas[1];
	url_param = {
		email: req.params.email,
		room: req.params.room
	};
	res.redirect('/');
})
chat_app.get('/chat/:email/:room', function (req, res) {
	var mas = req.url.split('/');
	console.log(mas);
	chat_param = {
		email: req.params.email,
		room: req.params.room
	};
	url_d = '/';
	res.redirect('/');

})


chat_app.get('/registration', function (req, res) {
	url_d = req.url;
	res.redirect('/');
})
chat_app.get('/chat', function (req, res) {
	url_d = req.url;
	res.redirect('/');
});

chat_app.get('/dashboard', function (req, res) {
	url_d = req.url;
	res.redirect('/');
});
db.on('error', console.error);

db.on('open', function () {
	console.log('open');

})

db.on('open', function () {
	console.log('db open');
	var collection = db.collection('Users');
	// registration
	var UsersRegistration = mongoose.model('UsersRegistration', model.UsersRegistrationSchema);
	var timerSch = mongoose.model('timerSch', model.timerSchema);
	var messages_story = mongoose.model('MessageStory', model.messageStory);

	io.on('connection', function (socket) {
		var name_res = false;
		count++;
		io.emit('updateUsers', count); // дописать
		socket.emit('router', url_d);
		api.joinRoom(socket, 'room');
		url_d = '/';
		console.log(url_d);
		if (url_param != "") {
			console.log("url_param");
			socket.on('joinToChat', function (data, fn) {
				console.log(url_param);
				fn(url_param);
				console.log('join to chat post event');
				url_param = "";
			})
		}
		console.log('chat_param ', chat_param);
		if (chat_param !== '') {
			var room = chat_param.room;
			var email = chat_param.email;
			socket.on('joinToRoom', function (data, fn) {
				console.log('chat_param 2', chat_param);
				UsersRegistration.find({ email: email }, function (err, mas) {
					console.log('joinToRoom ', mas);
					if (mas == false) return console.log('empty mas chat param');
					mas[0].room = room;
					api.joinRoom(socket, room);
					mas[0].save(function (err) {
						if (err) return console.log(err);
						else {
							fn(mas[0]);
							//chat_param = "";
						}
					})
				})
			})
		}

		UsersRegistration.find({}, function (err, res) {
			if (err) return console.log(err);
			for (let i = 0; i < res.length; i++) {
				let countToday = 0;
				let countWeek = 0;
				let countMonth = 0;
				timerSch.find({ email: res[i].email }, function (err, mas) {
					if (err) return console.log(err);
					if (mas == false) {
						res[i].tymerToday = countToday;
						res[i].tymerWeek = countWeek;
						res[i].typerMonth = countMonth;
					}
					for (var j = 0; j < mas.length; j++) {
						var date = new Date(mas[j].date)
						if (date.getDate() == new Date().getDate() && date.getHours() >= new Date().getHours() - 24) {
							countToday++;
						}
						if (date.getDate() >= new Date().getDate() - 7 && date.getMonth() === new Date().getMonth()) {
							countWeek++;
						}
						if (date.getMonth() === new Date().getMonth()) {
							countMonth++;
						}
					}
					res[i].tymerToday = countToday;
					res[i].tymerWeek = countWeek;
					res[i].typerMonth = countMonth;
					res[i].save(function (err, mes) {
						if (err) return console.log(err);
					})
				})
			}
		}).stream().on('close', function () {
			console.log('stream close init');
		})

		socket.on('updateUsersList', function () {
			UsersRegistration.find({ room: result[0].room }, function (err, arr) {
				io.sockets.in(api.currentRoom[socket.id]).emit("updateSocketList", arr);
			}).stream().on('close', function () {
				console.log('stream close updateUsersList');
			})
		})
			.on('active', function (data) {
				api.user_email = data.email;
				UsersRegistration.find({ email: data.email }, function (err, res) {
					if (err) return console.log(err);
					if (res == false) return console.log('empty array');
					api.currentRoom[socket.id] = data.room;
					res[0].online = true;
					res[0].save(function (err) {
						if (err) return console.log(err);
						else return console.log('success connect');
					})
				}).stream().on('close', function () {
					console.log('stream close active');
				})
			})
			.on('stripe', function (msg, fn) {
				var charge = stripe.charges.create({
					amount: '500',
					currency: 'usd',
					source: msg.token,
					description: "Example charge"
				}, function (err, charge) {
					if (err && err.type === 'StripeCardError') {
						console.log('the card has been declined');
						console.log(err);
					}
				})
			})
			.on('updateListUsers', function (msg, fn) {
				io.emit('updateUsers', count);
				UsersRegistration.find({ room: api.currentRoom[socket.id] }, function (err, arr) {
					io.sockets.in(api.currentRoom[socket.id]).emit("updateSocketList", arr);
					fn(arr);
				}).stream().on('close', function () {
					console.log('stream close updateListUsers');
				})
			}).on('chatMessageToSocketServer', function (msg, func) {
				let name = api.nickNames[socket.id];
				let sockectObj = { name: name, msg: msg.data }
				var message = new messages_story({
					date: new Date(),
					email: msg.email,
					room: msg.room,
					message: msg.message,
					login: msg.login,
					src: "https:" + gravatar.url(msg.email) + "?d=identicon"
				})
				message.save(function (err, mes) {
					if (err) return console.log(err);
					// socket.broadcast.to(api.currentRoom[socket.id]).emit('broadcastToAll_chatMessage', message);
					messages_story.find({ room: api.currentRoom[socket.id] }, function (err, res) {
						if (err) return console.log(err);
						io.sockets.in(api.currentRoom[socket.id]).emit('mes_story', res);
					})
					func(msg);
				})/*.stream().on('close', function () {
				console.log('stream close');
			})*/
			})
			.on("mesStory", function (data, fn) {
				messages_story.find({ room: api.currentRoom[socket.id] }, function (err, res) {
					if (err) return console.log(err);
					io.sockets.in(api.currentRoom[socket.id]).emit('mes_story', res);
				}).stream().on('close', function () {
					console.log('stream close mesStory');
				})
			})
			.on('registration', function (msg, fn) {
				var base_url = "//www.gravatar.com/avatar/";
				var user = new UsersRegistration({
					name: msg.name,
					email: msg.email,
					password: api.hash(msg.password),
					roomId: socket.id,
					room: msg.room,
					tymerToday: 0,
					tymerWeek: 0,
					typerMonth: 0,
					online: true,
					pro_user: msg.pro_user,
					src: "https:" + gravatar.url(msg.email) + "?d=identicon"
				});
				socket.leave('room');
				api.joinRoom(socket, msg.room);
				UsersRegistration.find({ name: msg.name, email: msg.email }, function (err, arr) {
					if (err) return console.log(err);
					console.log(arr);
					if (arr == false) {
						user.save(function (err, user) {
							if (err) return console.log(err);
							//clientListNames.push(msg.name);
							api.nickNames[socket.id] = msg.name;
							api.currentRoom[socket.id] = msg.room;
							fn('true', msg.name);
							io.emit('updateUsers', count);
						})
					} else
						fn('Name or username is already taken ', '');

				}).stream().on('close', function () {
					console.log('stream close registration');
				})
			})
			.on('login_', function (msg, fn) {
				socket.emit('updateUsers', count);
				UsersRegistration.find({ email: msg.email }, function (err, arr) {
					if (err) return console.log(err);
					if (arr == false)
						fn('error ', msg.email);
					else {
						api.nickNames[socket.id] = arr[0].name;
						api.currentRoom[socket.id] = arr[0].room;
						api.joinRoom(socket, arr[0].room);
						fn('true', arr[0]);
						UsersRegistration.find({ room: api.currentRoom[socket.id] }, function (err, arr) {
							if (err) return console.log(err);
							for (var i = 0; i < arr.length; i++) {
								clientListNames.push(arr[i].name)
							}
							io.sockets.in(api.currentRoom[socket.id]).emit("updateSocketList", arr);
						})
					}
				}).stream().on('close', function () {
					console.log('stream close login');
				})
			})
			.on('CurrentRoomUser', function (msg, fn) {
				UsersRegistration.find({ room: api.currentRoom[socket.id] }, function (err, arr) {
					if (err) return console.log(err);
					for (var i = 0; i < arr.length; i++) {
						clientListNames.push(arr[i].name)
					}
				})
			})
			.on('UpdateTimer', function (email, fn) {
				UsersRegistration.find({ email: email }, function (err, arr) {
					if (err) return console.log(err);
					if (arr == false) return console.log('empty update array');
					arr[0].tymerToday++;
					arr[0].tymerWeek++;
					arr[0].typerMonth++;
					arr[0].save(function (err) {
						if (err) return console.log(err);
						else {
							fn(arr[0]);
						}
					})
				}).stream().on('close', function () {
					console.log('stream close updateTimer');
				});
			})
			.on('timerUp', function (data, fn) {
				timerSch.find({ email: data.email }, function (err, arr) {
					if (err) return console.log(err);
					fn(arr);
				}).stream().on('close', function () {
					console.log('stream close timerUp');
				})
			})
			.on('sendEmail', function (data, fn) {
				var str_send;
				console.log(data);
				var email = data.emailTo;
				var room = data.room;
				UsersRegistration.find({ email: data.emailTo }, function (err, arr) {
					var ref = data;
					if (err) return console.log(err);
					console.log(data);
					if (arr.length === 0) {
						str_send = `
						<style>
					a{
						text-decoration: none;
					}
						</style>
						Follow the link to create an account : <a href="https://sheltered-springs-88229.herokuapp.com/registration/${email}/${room}">link</a>
					`;
					}
					else if (arr[0].room == 'noRoom') {
						str_send = `
					<style>
						a{
						text-decoration: none;
					}
					</style>
							You are invited into the room ${room}, to accept the invitation to go to <a href="https://sheltered-springs-88229.herokuapp.com/chat/${email}/${room}">Link</a>
						`;
					}


					var mailgun = new Mailgun({ apiKey: api_key, domain: domain });
					var data = {
						from: 'Excited User <me@samples.mailgun.org>',
						to: email,
						subject: '',
						html: str_send
					};
					mailgun.messages().send(data, function (error, body) {
						console.log(body);
						console.log(error);
					});
				}).stream().on('close', function () {
					console.log('stream close send email');
				})
			})
			.on('timerTo', function (data) {
				var mas = [];
				var count = 0;
				if (data) {
					var object = new timerSch({
						date: new Date(),
						email: data.email,
						room: data.room
					});
					UsersRegistration.find({}, function (user) {
						object.save(function (err, timer) {
							if (err) return console.log(err);
							timerSch.find({ email: data.email }, function (err, res) {
								if (err) return console.log(err);
								if (res == false) return console.log('empty');

								for (var i = 0; i < res.length; i++) {
									var date = new Date(res[i].date)
									if (date.getDate() == new Date().getDate()) {
										mas.push(res[count]);
										count++;
									}
								}
								io.emit('updateUserRaiting', mas);
							})
						})
					}).stream().on('close', function () {
						console.log('stream close timerTo');
					})
				}
			})
			.on('timerOn', function (data) {
				timerSch.find({ email: data.email }, function (err, res) {
					if (err) return console.log(err);
					var mas = [];
					var count = 0;
					for (var i = 0; i < res.length; i++) {
						var date = new Date(res[i].date);
						if (date.getDate() == new Date().getDate()) {
							mas.push(res[count]);
							count++;
						}
					}
					io.emit('updateUserRaiting', mas);
				}).stream().on('close', function () {
					console.log('stream close timerOn');
				})
			})
			.on("chatRooms", function (roomData, fn) {
				var isRoom = false;
				UsersRegistration.find({}, function (err, arr) {
					if (err) return console.log(err);
					for (var i = 0; i < arr.length; i++) {
						if (arr[i].room === roomData) {
							isRoom = true;
							break;
						}
						else
							isRoom = false;
					}
					//	io.emit('roomIsExist', isRoom);
					fn(isRoom);
				}).stream().on('close', function () {
					console.log('stream close charRooms');
				})
			})
			.on('timerUsersRaiting', function () {
				timerSch.find({ room: api.currentRoom[socket.id] }, function (err, res) {
					if (err) return console.log(err);
					// write event timer follow rauting
				}).stream().on('close', function () {
					console.log('stream close timerUsersRaiting');
				})
			})
			.on('deleteUser', function (email, fn) {
				UsersRegistration.find({ email: email }, function (err, res) {
					if (err) return console.log(err);
					res[0].room = "noRoom";
					res[0].save(function (err) {
						if (err) return console.log(err);
						else {
							fn(true);
							io.sockets.in(api.currentRoom[socket.id]).emit('del', res[0]);
						}
					})
				})
			})
			.on('disconnect', function (data) {
				count--;
				socket.emit('updateUsers', count);
				UsersRegistration.find({ name: api.nickNames[socket.id] }, function (err, res) {
					if (err) return console.log(err);
					if (res == false) return console.log('empty array disc1');
					res[0].online = false;
					res[0].save(function (err) {
						if (err) return console.log(err);
						else {
							UsersRegistration.find({ room: api.currentRoom[socket.id] }, function (err, arr) {
								if (err) return console.log(err);
								if (arr == false) return console.log('empty array disc');
								io.sockets.in(api.currentRoom[socket.id]).emit("updateSocketList", arr);
							})
						}
					})

				}).stream().on('close', function () {
					console.log('stream close disconnect');
				})
			});
		chat_param = "";
	});
	console.log('connection to db');
})
var options = {
	server: { socketOptions: { connectTimeoutMS: 30000, keepAlive: 3000 } }, auto_reconnect: true, poolSize: 500,
	autoIndex: true
}
mongoose.connect('mongodb://localhost:27017/timerProject', options);

http.listen(process.env.PORT, function () {
	console.log('listening on *: ' + process.env.PORT);
});


