var mongoose = require('mongoose');
exports.UsersRegistrationSchema = new mongoose.Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    email: String,
    password: {
        type: String,
        required: true
    },
    roomId: String,
    room: String,
    tymerToday: Number,
    tymerWeek: Number,
    typerMonth: Number,
    online: Boolean,
    admin: Boolean,
    src: {
        type: String
    },
    pro_user: {
        type: Boolean
    }
});
// timer count
exports.timerSchema = new mongoose.Schema({
    date: {
        type: Date,
        default: Date.now,
        unique: true
    },
    email: {
        type: String,
    },
    room: String
}, {
        autoIndex: true
    });
// message stoty
exports.messageStory = new mongoose.Schema({
    date: {
        type: Date,
        default: Date.now()
    },
    email: {
        type: String
    },
    room: {
        type: String
    },
    message: {
        type: String
    },
    login: {
        type: String
    },
    src: {
        type: String
    }
})